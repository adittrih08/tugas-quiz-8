import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TC-Products/TC10-Add multiple product to cart'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Product Page/cart'), 0)

Mobile.tap(findTestObject('Checkout Page/buttonCheckout'), 0)

Mobile.setText(findTestObject('Checkout Page/inputName'), 'Aditya Tri Herdiansyah', 0)

Mobile.setText(findTestObject('Checkout Page/inputAddress1'), 'Vila Dago', 0)

Mobile.setText(findTestObject('Checkout Page/inputAddress2'), 'Pamulang', 0)

Mobile.setText(findTestObject('Checkout Page/inputCity'), 'Tangerang Selatan', 0)

Mobile.setText(findTestObject('Checkout Page/inputState'), 'Banten', 0)

Mobile.scrollToText('United Kingdom', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Checkout Page/inputZipCode'), '14231', 0)

Mobile.setText(findTestObject('Checkout Page/inputCountry'), 'Indonesia', 0)

Mobile.tap(findTestObject('Checkout Page/buttonPayment'), 0)

Mobile.setText(findTestObject('Checkout Page/inputNameCardHolder'), 'Aditya Tri Herdiansyah', 0)

Mobile.setText(findTestObject('Checkout Page/inputCardNumber'), '1234 5678 9123 456', 0)

Mobile.setText(findTestObject('Checkout Page/inputExpDate'), '04/24', 0)

Mobile.setText(findTestObject('Checkout Page/inputSecurityCode'), '123', 0)

Mobile.tap(findTestObject('Checkout Page/buttonReviewOrder'), 0)

Mobile.tap(findTestObject('Checkout Page/buttonPlaceOrder'), 0)

Mobile.verifyElementText(findTestObject('Checkout Page/verifCheckoutComplete'), 'Checkout Complete')

