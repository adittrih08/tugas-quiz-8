<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login Test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>15911bd9-fb70-49e8-8897-b5ba87aa0b55</testSuiteGuid>
   <testCaseLink>
      <guid>5ecc78f7-6e88-4aea-ad36-fd7368430367</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-Login/TC01-Open login page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9b65ba6d-8c33-4aef-bc1d-5441da16e1e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-Login/TC02-Login with valid user</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>438ccc06-8f30-426e-8ccd-011f5b867880</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-Login/TC03-Login with locked user</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ed9bd866-3596-4994-93b3-0355d3be9c4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-Login/TC04-Login with invalid user</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>52526df0-0324-449d-a3fd-e9fc42b82389</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-Login/TC06-Login without input password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>63ce9a9b-8538-4910-a6f6-56c94790ba82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-Login/TC05-Login without input username</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fba1d5fe-dc62-491e-87f2-42d51d86d766</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-Login/TC07-Login with data binding</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>420371fe-e406-49db-9e03-8e7607f85398</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/loginData</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>420371fe-e406-49db-9e03-8e7607f85398</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>f8ce39fe-9792-4b1d-8798-2d45112ef68b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>420371fe-e406-49db-9e03-8e7607f85398</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>e4d24dcc-0d1d-4c65-8ccc-a90bfc5f79f6</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
